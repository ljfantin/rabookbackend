var path = require('path');
var express = require('express');
var timeout = require('connect-timeout')
var responseTime = require('response-time')

var app = express();
var router = express.Router();

var db_manager = require('./classes/db_manager.js');
var objects_controller = require('./classes/objects_controller.js');

var bodyParser = require('body-parser');
var methodOverride = require("method-override");


var port = process.env.PORT || 5000;

app.set('port', port);
app.use(bodyParser.urlencoded({ extended: false })); 
app.use(bodyParser.json()); 
app.use(methodOverride());
app.use(timeout('120s'));
app.use(responseTime())
app.use('static',express.static(path.join(__dirname, 'resources')));

app.use('/', router);


router.use(function (req, res, next) {
  console.log('Request Time:', Date.now());
  next();
});

router.route('/book/:client_id/raobject')
.get(objects_controller.findAllRaObjectForClientAndScreenSize)
.post(objects_controller.addRaObject);

app.get('/', function (req, res) {
  res.send('Hello World!');
});

router.route('/screensize/:screen_size')
  .get(objects_controller.getBestScreenSize);

app.use(function(err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

app.listen(port, function () {
  console.log('Example app listening on port 5000!');
});
