var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var RaObject = mongoose.model('RaObject');
var Rectangle = require("rectangle-node");

//GET - Return all pictures for client id and screen size in the DB
exports.findAllRaObjectForClientAndScreenSize = function(req, res) { 

    var screenSizeParam = req.headers.screen_size;
    findCorrectSize(screenSizeParam, function(err, screenSize) {
        if (err) {
            return res.send(500, err.message);
        } else {
            var landscape = new Rectangle(0,0, screenSize.h, screenSize.w);
            var landscapeSize = getScreenSizeFromRectangle(landscape);
            var portraitSize = getScreenSizeFromRectangle(screenSize);
            console.log('landscape size: ',landscapeSize);
            console.log('portraitSize size: ',portraitSize);

            RaObject.find({
                $and: [ {client_id: req.params.client_id}, { platform: req.headers.platform}, { $or: [ {screen_size: portraitSize}, {screen_size: landscapeSize} ] } ] 

            }, function(err, objects) {
                if(err) {
                    return res.send(500, err.message);
                } else {
                    console.log('cantidad de objectos: ', objects.length);
                    return res.status(200).json(objects);
                }
            });
        }
    });
/*
    RaObject.find({
                client_id: req.params.client_id,
                screen_size: req.headers.screen_size,
                platform: req.headers.platform

            }, function(err, objects) {
                if(err) {
                    res.send(500, err.message);
                } else {
                    res.status(200).jsonp(objects);
                }
    });*/
};

exports.addRaObject = function(req, res) {  

    var object = new RaObject({
        client_id:    req.body.client_id,
        platform: req.headers.platform, 
        screen_size:  req.headers.screen_size,
        layer_type: req.body.layer_type,
        layer_identifier: req.body.layer_identifier,
        picture_url:  req.body.picture_url,
        picture_height: req.body.picture_height,
        picture_width: req.body.picture_width,
        keypoints: req.body.keypoints,
        descriptors: req.body.descriptors
    });

    
    object.save(function (err) {

        if (err) {
            res.status(500).send( err.message);
        } else {
            res.status(200).send(object);
        }   
    });

};

exports.getBestScreenSize = function(req, res) {

    var screenSizeParam = req.params.screen_size;
    //var screenSize = req.headers.screen_size;

    findCorrectSize(screenSizeParam, function(err, screenSize) {
        console.log(screenSize);
        res.status(200).send(screenSize);
    });
};

function findCorrectSize(screenSize, callback)
{

    RaObject.collection.distinct("screen_size", function(err, arrayWithSizes){
        if(err) {
            callback(err,null);
            return;
        } else {

            var rectangleDeviceScreenSize = getRectangleFromScreenSize(screenSize);
            var bestRectangleScreenSize;
            arrayWithSizes.forEach(function(value){
                var rectangleScreenSize = getRectangleFromScreenSize(value)
                // si lo encuentro entonces ya esta retorno ese 
                if (rectangleScreenSize.equals(rectangleDeviceScreenSize)) {
                    bestRectangleScreenSize = rectangleDeviceScreenSize;
                    return;
                } else {
                    // sino itero y me quedo con el mas cerca
                    if (!bestRectangleScreenSize) {
                        bestRectangleScreenSize = rectangleScreenSize;
                    } 
                    else // si el rectangulo actual tiene mas interseccion con el rectangulo pasado como parametro, que el rectangulo que tenia guardado como mejor => me quedo con ese nuevor ectangulo e itero 
                    if (rectangleDeviceScreenSize.intersection(rectangleScreenSize).area() > rectangleDeviceScreenSize.intersection(bestRectangleScreenSize).area()) {
                        bestRectangleScreenSize = rectangleScreenSize;
                    }
                }
            });
            console.log("se eligio 2 :",bestRectangleScreenSize);
            //var screenSizeResult = bestRectangleScreenSize.w.toString()+'x'+bestRectangleScreenSize.h.toString();
            //console.log("se eligio:",screenSizeResult);
            callback(null, bestRectangleScreenSize);
            return;
        }
    });
}

function getRectangleFromScreenSize(screenSize) 
{
    var splitValues = screenSize.split('x');
    var width = Number(splitValues[0]);
    var height = Number(splitValues[1]);
    return new Rectangle(0,0, width, height);
}

function getScreenSizeFromRectangle(rectangle) 
{
    var width = rectangle.w;
    var height = rectangle.h;
    var screenSize = width+'x'+height;
    return screenSize;
}



