
var mongoose = require('mongoose');

// mongodb://<dbuser>:<dbpassword>@ds145639.mlab.com:45639/rabook_pictures
var urlLocalConnection = 'mongodb://localhost/rabook';
//var urlRemoteConnection = 'mongodb://fidelin:babasonicos13@ds145639.mlab.com:45639/rabook';
var urlRemoteConnection = 'mongodb://fidelin:babasonicos13@ds155820.mlab.com:55820/rabook';

mongoose.connect(urlRemoteConnection, function(err, res) {  
  if(err) {
    console.log('ERROR: connecting to Database. ' + err);
  }
});

var Schema = mongoose.Schema;

var objectsSchema = new Schema({ 
 client_id: { type: String },
 platform: { type: String, enum: ['android', 'ios'] },
 screen_size: { type: String, enum: ['320x480', '640x960', '640x1136', '750x1334', '1125x2001', '1242x2208', '1536x2048', '2048x2732', '480x320', '960x640', '1136x640', '1334x750', '2001x1125', '2208x1242', '2048x1536', '2732x2048']},
 layer_type: { type: String, enum: ['vimeo', 'object', 'url']},
 layer_identifier: {type: String},
 picture_url: {type: String},
 picture_width: {type: Number, default:0},
 picture_height: {type: Number, default:0},
 keypoints: {type: Array, default:[]},
 descriptors: {type: Array, default:[]}
});

/*
var Temp = mongoose.model('Temp', objectsSchema);


var arrayWithSizes = Temp.schema.path('screen_size').enumValues;

module.exports = mongoose.model('RaObject', objectsSchema);
*/

module.exports = mongoose.model('RaObject', objectsSchema);

